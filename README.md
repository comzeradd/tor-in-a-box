<img src="resources/logo.png" width="150">

Ansible scripts for setting up a <a href="https://trac.torproject.org/projects/tor/wiki/TorRelayGuide">Tor relay</a>.

## Requirements

1. Ansible (duh..)

## Usage

Open `hosts` files and make sure it has the right values.

Then just run:

```
ansible-playbook -v main.yml
```

## Support

[![Liberapay](resources/liberapay.svg)](https://liberapay.com/comzeradd/donate)

## License

[![license](https://img.shields.io/badge/license-AGPL%203.0-6672D8.svg)](LICENSE)
